package main

import (
	"fmt"
	"regexp"
	"strings"
)

func no3(word string, length int) []string {
	reg, _ := regexp.Compile("[^a-zA-Z0-9 ]+")

	wordArray := strings.Split(reg.ReplaceAllString(word, ""), " ")
	var result []string

	for _, v := range wordArray {
		if len(v) == length {
			result = append(result, v)
		}
	}

	return result
}

func no2(nums []int, x int) []int {
	var result []int
	for _, v := range nums {
		isSame := false
		for _, s := range nums {
			if s != 0 && (v/s == x) {
				isSame = true
				break
			}
		}

		if !isSame {
			result = append(result, v)
		}
	}

	return result
}

func no1(nums []int) []int {
	var result []int

	for _, v := range nums {
		isMoreThanZero := true
		for _, s := range nums {
			if v-s < 0 {
				isMoreThanZero = false
				break
			}
		}

		if isMoreThanZero {
			result = append(result, v)
		}
	}

	return result
}

func main() {
	fmt.Println("isi dari no 1: ", no1([]int{-4, 2, -16, 3, -100, 4, 4, 16, 16, 1}))
	fmt.Println("isi dari no 2: ", no2([]int{8, 10, 3, 4, 5, 2, -10, -2}, 5))
	fmt.Println("isi dari no 3: ", no3("saya naik moto ,,,, ke pasar bersama ibu dan bapak saya", 4))

}
